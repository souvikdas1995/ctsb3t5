import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(input: string, ...args: unknown[]): unknown {
    return this.firstChar(input) + this.remainingString(input);
  }

  firstChar(input:string):string{
    console.log("reading first char")
    return input.charAt(0).toUpperCase();
  }
  
  remainingString(input:string):string{
    console.log("reading entire string after first char")
    return input.substring(1).toLowerCase();
  }

}
