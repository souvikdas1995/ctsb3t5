import { Component } from "@angular/core";

@Component({
    selector: "app-root",
    // template: "<h1>This is Component</h1>",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.scss"]
  })
export class AppComponent{
  today:Date = new Date();
  now:number = Date.now();
  hero:string = "bAtMAn";
}