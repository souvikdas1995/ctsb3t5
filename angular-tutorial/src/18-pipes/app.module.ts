import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { CapitalizePipe } from './pipes/capitalize.pipe';

  
  @NgModule({
    declarations: [AppComponent, CapitalizePipe], // comp, pipe, directive
    exports: [], // provide modules for others
    imports: [BrowserModule], // consume modules from others
    providers: [], // utils, reusable code
    bootstrap: [AppComponent]
  })
export class AppModule{}