import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path: "", component: UserComponent,
    children: [
      { path: "login", component: LoginComponent },
      { path: "welcome", component: WelcomeComponent },
      { path: "**", redirectTo: "login" }
    ]
  }

]

@NgModule({
  declarations: [
    LoginComponent,
    WelcomeComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class UserModule { }
