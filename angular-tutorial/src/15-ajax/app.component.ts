import { HttpResponse } from "@angular/common/http";
import { Component } from "@angular/core";
import { Movie } from "./models/movie.model";
import { MovieService } from "./services/movie.service";

@Component({
  selector: "app-root",
  // template: "<h1>This is Component</h1>",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {

  movies: Movie[] = []
  selectedMovie: Movie | null = null;

  constructor(private movieService: MovieService) { }

  loadMovies() {
    this.movieService.getAllMovies().subscribe({
      next: (res:any) => {
        console.log()
        console.log("movies are fetched")
        this.movies = res;
        console.log(this.movies)
      },
      error: (err) => {
        console.log("something bad happened")
        console.log(err)
      }
    })
  }

  editMovie(id: number) {
    // this.router.navigate(["/path/id"]);
    this.movieService.getMovieById(id).subscribe({
      next: (res: any) => {
        this.selectedMovie = res;
      }
    })
  }

  deleteMovie(id: number) {
    this.movieService.deleteMovieById(id).subscribe({
      next: (res: any) => {
        this.loadMovies();
      }
    })
  }

  saveMovie(){
    let movie = new Movie(0, "Spriderman", "Iron man", 5.0);
    this.movieService.saveMovie(movie).subscribe({
      next: (res: any) => {
        this.loadMovies();
      }
    })
  }


}