import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Movie } from "../models/movie.model";

@Injectable({"providedIn": "root"})
export class MovieService{

    constructor(private http:HttpClient){}

    getAllMovies(){
        console.log("Fetching all movies")
        return this.http.get("http://localhost:8080/api/v1/movies");
    }

    getMovieById(id:number){
        return this.http.get("http://localhost:8080/api/v1/movies/"+id);
    }

    deleteMovieById(id:number){
        return this.http.delete("http://localhost:8080/api/v1/movies/"+id);
    }

    saveMovie(movie:Movie){
        return this.http.post("http://localhost:8080/api/v1/movies", movie);

        // let token = localStorage.getItem("token")
        // return this.http.post("http://localhost:8080/api/v1/movies", movie, {headers: {"Authorization": "bearer "+token}});
    }

}