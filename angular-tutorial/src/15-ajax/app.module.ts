import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";

  
  @NgModule({
    declarations: [AppComponent], // comp, pipe, directive
    exports: [], // provide modules for others
    imports: [BrowserModule, HttpClientModule], // consume modules from others
    providers: [], // utils, reusable code
    bootstrap: [AppComponent]
  })
export class AppModule{}