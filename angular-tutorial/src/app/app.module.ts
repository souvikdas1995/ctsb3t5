import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComonent } from "./app.component";



@NgModule({
    declarations: [AppComonent], // comp, direc, pipes
    providers: [], //services
    imports: [BrowserModule],// other modules
    exports: [],  // export your modules
    bootstrap: [AppComonent]
})
export class AppModule { }