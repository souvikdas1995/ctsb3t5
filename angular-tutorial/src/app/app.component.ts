import { Component } from "@angular/core";

@Component({
    selector: "app-root",
    // template: "<h1>This is root component</h1>",
    templateUrl: "app.component.html",
    // styles: ["h1{text-align: center;background: black;color: white;}"]
    styleUrls: ["app.component.css"]
})
export class AppComonent { }