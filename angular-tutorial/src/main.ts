// import { AppModule } from "./app/app.module";
// import { AppModule } from "./01-basic/app.module";
// import { AppModule } from "./02-events/app.module";
// import { AppModule } from "./03-bindings/app.module";
// import { AppModule } from "./04-child-comp/app.module";
// import { AppModule } from "./06-forms/app.module";
// import { AppModule } from "./08-spa/app.module";
// import { AppModule } from "./09-nest-routing/app.module";
import { AppModule } from "./10-nest-router-outlet/app.module";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic"

console.log("This is main file")

platformBrowserDynamic().bootstrapModule(AppModule);