import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { min } from "rxjs";

@Component({
    selector: "app-root",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.scss"]
  })
export class AppComponent{
  loginForm:FormGroup; 
  
  constructor(){
    this.loginForm = new FormGroup({
      username: new FormControl("", [
        // Validators.pattern(""),
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(8)
      ]),
      password: new FormControl("")
    });
  }

  getLogin(){
    console.log(this.loginForm)
    console.log(this.loginForm.value)

    // some other
    // send to server
  }
}