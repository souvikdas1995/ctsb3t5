import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/13-service/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // @Autowired
  // Router router;

  constructor(private router:Router, private userService:UserService) { }

  ngOnInit(): void {
  }

  getLogin(name:string){
    console.log("Current name: "+name);

    // send request to server
    // res will be back -> valid or invalid user

    // assuming it as valid user

// save data in service      
      this.userService.user = name;

      this.router.navigate(["welcome"]);
  }

}
