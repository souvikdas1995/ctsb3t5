import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userObject = {
    valid: false,
    name: ""
  }

  constructor() { }

  set user(name:string){
    this.userObject.valid = !!name;
    this.userObject.name = name;
  }

  get user(){
    return this.userObject.name;
  }

  get isValid(){
    return this.userObject.valid;
  }
  
}
