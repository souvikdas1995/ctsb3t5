import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { RouterModule, Routes } from "@angular/router";

const routes:Routes = [
  { path: "home", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "login", component: LoginComponent },

  { path: "**", redirectTo: "home" }
]
  
  @NgModule({
    declarations: [
      AppComponent, 
      HeaderComponent, FooterComponent, HomeComponent, AboutComponent, LoginComponent
    ], // comp, pipe, directive
    exports: [], // provide modules for others
    imports: [
      BrowserModule,
      RouterModule.forRoot(routes)
    ], // consume modules from others
    providers: [], // utils, reusable code
    bootstrap: [AppComponent]
  })
export class AppModule{}