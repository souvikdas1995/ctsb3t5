import { HttpResponse } from "@angular/common/http";
import { Component } from "@angular/core";
import { MovieService } from "./services/movie.service";

@Component({
  selector: "app-root",
  // template: "<h1>This is Component</h1>",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {


  constructor(private movieService: MovieService) { }

  loadMovies() {
    this.movieService.getAllMovies().subscribe({
      next: (res:any) => {
        console.log()
        console.log("movies are fetched")
      }
    })
  }



}