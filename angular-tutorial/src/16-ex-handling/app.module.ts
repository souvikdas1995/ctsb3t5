import { HttpClientModule } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { MyErrorHandler } from "./services/error-handling";

  
  @NgModule({
    declarations: [AppComponent], // comp, pipe, directive
    exports: [], // provide modules for others
    imports: [BrowserModule, HttpClientModule], // consume modules from others
    providers: [{useClass: MyErrorHandler, provide: ErrorHandler}], // utils, reusable code
    bootstrap: [AppComponent]
  })
export class AppModule{}