import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({"providedIn": "root"})
export class MovieService{

    constructor(private http:HttpClient){}

    getAllMovies(){
        console.log("Fetching all movies")
        return this.http.get("https://jsonplaceholder.typicode.com/images");
    }


}