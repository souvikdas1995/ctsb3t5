import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WelcomeGuard implements CanActivate {
  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log("In welcome guard.");
    return true;
  }
  
}
