import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // @Autowired
  // Router router;

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  getLogin(name:string){
    console.log("Current name: "+name);

    // send request to server
    // res will be back -> valid or invalid user

    // assuming it as valid user
    if(!!name){
      this.router.navigate(["welcome", name]);
    } else {
      console.log("Invalid user")
    }
  }

}
