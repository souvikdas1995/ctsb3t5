import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {

  /*
  class Controller{

    @Autowired
    Service service;

  }

  @Injectable({providedIn: 'root'})
  class Service{}
  */

  public username:string = "";
  constructor(private activatedRoute:ActivatedRoute) { 
    this.activatedRoute.params.subscribe((params)=>{
      // console.log("param changed");
      console.log(params);
      this.username = params['username'];
    })
  }

}
