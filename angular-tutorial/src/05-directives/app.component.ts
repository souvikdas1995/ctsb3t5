import { Component } from "@angular/core";

@Component({
    selector: "app-root",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.scss"]
  })
export class AppComponent{
    myCssClass:string = "success";
    flag:boolean = true;

    myStyleCss:any = {
      "text-align": "right"
    }

    emps:any[] = [];

    toggle(){
      this.flag=!this.flag;

      if(this.flag){
        this.myCssClass = "success";

        this.myStyleCss["text-align"]= "right";

  
      } else {
        this.myCssClass = "error";

        this.myStyleCss["text-align"]= "left";
      }

      this.emps.push({name: "mark", age: 25, address: "tokyo"});
      this.emps.push({name: "miley", age: 28, address: "new york"});
      this.emps.push({name: "emily", age: 23, address: "punjab"});
      this.emps.push({name: "carl", age: 31, address: "delhi"});


    }
  }