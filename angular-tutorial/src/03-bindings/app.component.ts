import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {
  subject:string = "nothing";

  update(sub:string):void{
    this.subject = sub;
  }
}