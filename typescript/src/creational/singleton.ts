export class Calculator{
    private static obj:Calculator = new Calculator();
    private constructor(){}

    public add(a:number, b:number):number{
        return a+b;
    }

    public static getInstance():Calculator{
        return this.obj;
    }
}