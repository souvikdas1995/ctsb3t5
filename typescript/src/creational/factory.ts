abstract class Computer{
    public abstract getRam():string;
    public abstract getHdd():string;
    public abstract getCpu():string;
    
    public specs():string{
        return `RAM: ${this.getRam()}, HDD: ${this.getHdd()}, CPU: ${this.getCpu()}`;
    }
}

class Laptop extends Computer{
    // private ram:string;
    // private hdd:string;
    // private cpu:string;
    constructor(private ram:string="4GB", private hdd:string="1TB", private cpu:string="4"){
        super();
        // this.ram = r;
    }
    public getRam(): string {
        return this.ram;
    }
    public getHdd(): string {
        return this.hdd;
    }
    public getCpu(): string {
        return this.cpu;
    }
    
}

class Server extends Computer{
    constructor(private ram:string="64GB", private hdd:string="10TB", private cpu:string="32"){
        super();
    }
    public getRam(): string {
        return this.ram;
    }
    public getHdd(): string {
        return this.hdd;
    }
    public getCpu(): string {
        return this.cpu;
    }
    
}


class ComputerFactory{
    public static getComputer(type:string):Computer{
        if(type.toLowerCase().trim()==="server"){
            return new Server();// assembling, networking
        } else if(type.toLowerCase().trim()==="laptop"){
            return new Laptop();// assemble
        } else {
            return null;
        }
    }
}


let comp:Computer = ComputerFactory.getComputer("demo");
console.log(comp);

